package com.example.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        LogginButton.setOnClickListener {
            if (emailfield.text.isNotEmpty() && passfield.text.isNotEmpty()) {
                    val intent = Intent(this, secondactivity::class.java)
                    startActivity(intent)
            } else if (emailfield.text.isEmpty() && passfield.text.isNotEmpty()){
                Toast.makeText(this,"please fill Email field",Toast.LENGTH_LONG).show()
            }else if (emailfield.text.isNotEmpty() && passfield.text.isEmpty()){
                Toast.makeText(this,"please enter your password ",Toast.LENGTH_LONG).show()

            }else{
                Toast.makeText(this,"Please fill all fields",Toast.LENGTH_LONG).show()

            }
        }
    }


}




